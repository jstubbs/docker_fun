#!/bin/bash 
# Author: Joe Stubbs
# Date: April, 2014
# Description: Installs the Python 2.7.x stack from source

BASE="/home/apim"
INSTALL_HOME="$BASE/install_files"

install_python() {
  # Installs Python 2.7.3 from source
  sudo yum groupinstall "Development tools"
  sudo yum install zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel
  mkdir $INSTALL_HOME/python_install
  sudo chown jstubbs:jstubbs $BASE/
  cd $INSTALL_HOME/python_install
  wget http://python.org/ftp/python/2.7.3/Python-2.7.3.tar.bz2
  tar xf Python-2.7.3.tar.bz2
  cd Python-2.7.3
  ./configure --enable-shared --prefix=/usr/local
  sudo make && sudo make altinstall

  sudo cp /usr/local/lib/libpython2.7.so.1.0 /lib64/
}

install_dist_tools() {
  # Installs distribute and virtualenv
  cd $INSTALL_HOME/python_install
  wget http://pypi.python.org/packages/source/d/distribute/distribute-0.6.35.tar.gz --no-check-certificate
  tar xf distribute-0.6.35.tar.gz
  cd distribute-0.6.35
  sudo /usr/local/bin/python2.7 setup.py install

  sudo /usr/local/bin/easy_install-2.7 virtualenv

  # create virtualenvs directory:
  mkdir $BASE/virtualenvs
}

install_mod_wsgi() {
  # Installs mod_wsgi 3.4; assumes httpd has been installed
  sudo yum install python-devel httpd-devel
  mkdir $INSTALL_HOME/mod_wsgi
  cd $INSTALL_HOME/mod_wsgi
  wget http://modwsgi.googlecode.com/files/mod_wsgi-3.4.tar.gz
  tar xzf mod_wsgi-3.4.tar.gz
  cd mod_wsgi-3.4/
  sudo ./configure --with-python=/usr/local/bin/python2.7
  sudo make
  sudo make install
}

#######################################
# Main program
#######################################

install_python
install_dist_tools
install_mod_wsgi
