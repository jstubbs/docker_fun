#!/bin/bash -ue
mkdir -p /nextflow/sum/data
cp /nextflow/mult_3/tmp/output /nextflow/sum/data/in.txt
docker run -v /nextflow/sum/data:/data jstubbs/sum
