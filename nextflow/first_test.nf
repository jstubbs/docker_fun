// nextflow script to do proof of concept "pipe" of docker containers \sum_x (5 + x)3.


add_5_input = file("/nextflow/add_5/input.txt")

process add_5 {
    input:
        file x from add_5_input
    output:
        file "/nextflow/add_5/data/output.txt" into add_output
    """
    mkdir -p /nextflow/add_5/data
    docker run -v /nextflow/add_5/data:/data -v /nextflow/add_5/input.txt:/data/input.txt jstubbs/add_n -i 5
    """

}

process mult_3 {

    input:
        file x from add_output
    output:
        file "/nextflow/mult_3/tmp/output" into mult_out
    """
    mkdir -p /nextflow/mult_3/tmp
    docker run -v /nextflow/mult_3/tmp:/tmp -v /nextflow/add_5/data/output.txt:/nextflow/mult_3/tmp/input jstubbs/mult_n -f 3
    """

}

process sum {

    input:
        file x from mult_out
    output:
        file "/nextflow/sum/data/out.txt" into sum_out
    """
    mkdir -p /nextflow/sum/data
    docker run -v /nextflow/sum/data:/data -v /nextflow/mult_3/tmp/output:/nextflow/sum/data/in.txt jstubbs/sum
    """

}