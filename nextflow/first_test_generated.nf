add_5_input_0 = file("/nextflow/add_5/input.txt")


process sum {

    input:
        file x_0 from 

    output:
        file "/nextflow/sum/data/out.txt" into sum_output_0

    """
    mkdir -p /nextflow/sum/data

    docker run -v /nextflow/sum/data:/data  -v /nextflow/mult_3/data/output:/data/in.txt  jstubbs/sum python sum.py

    """
}


process add_5 {

    input:
        file x_0 from add_5_input_0

    output:
        file "/nextflow/add_5/data/output.txt" into add_5_output_0

    """
    mkdir -p /nextflow/add_5/data

    docker run -v /nextflow/add_5/data:/data  -v /nextflow/add_5/input.txt:/data/input.txt  jstubbs/add_n python add_n.py -i 5

    """
}


process mult_3 {

    input:
        file x_0 from add_5_output_0

    output:
        file "/nextflow/mult_3/tmp/output" into mult_3_output_0

    """
    mkdir -p /nextflow/mult_3/tmp

    docker run -v /nextflow/mult_3/tmp:/tmp  -v /nextflow/add_5/data/output.txt:/tmp/input  jstubbs/mult_n python mult_n.py -f 3

    """
}

