import ldap

# General:
DEBUG = True
ADMINS = (
    ('Joe Stubbs', 'jstubbs@tacc.utexas.edu'),
    ('Rion Dooley', 'dooley@tacc.utexas.edu'),
)

# Static files:
STATIC_ROOT = '/home/jstubbs/agave-ldap/src/users/static'

# LDAP:
AUTH_LDAP_BIND_DN='uid=admin,ou=system'
AUTH_LDAP_BIND_PASSWORD='secret'
LDAP_BASE_SEARCH_DN='o=agaveapi'

#AUTH_LDAP_BIND_DN='cn=Manager, o=agaveapi'
#AUTH_LDAP_BIND_PASSWORD='cowhands7}mathematics'
#LDAP_BASE_SEARCH_DN='o=agaveapi'



#SMTP:
EMAIL_HOST = 'localhost'
EMAIL_PORT = 25
EMAIL_HOST_USER = None
EMAIL_HOST_PASSWORD = None
EMAIL_USE_TLS = False

#DATABASE:
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'apimgtdb',
        'USER': 'superadmin',
        'PASSWORD': '02G2NS3SEO7XU0YP1J28B0KYU36',
        'HOST': 'localhost',   # Or an IP Address that your DB is hosted on
        'PORT': '3306',
    },

#    'default': {
#        'ENGINE': 'django.db.backends.sqlite3', 
#        'NAME': '/home/jstubbs/agave-ldap/src/users/userAppDB',
#    },
    'ldap': {
        'ENGINE': 'ldapdb.backends.ldap',
        'NAME': 'ldap://wso2-elb.tacc.utexas.edu:10389',
#        'NAME': 'ldaps://agaveldap.tacc.utexas.edu:636', 
        'USER': AUTH_LDAP_BIND_DN,
        'PASSWORD': AUTH_LDAP_BIND_PASSWORD,
        'TLS': False,
        'CONNECTION_OPTIONS': {
            ldap.OPT_X_TLS_DEMAND: False,
        }
     }
}

# WSO2 configs:
APIM_STORE_SERVICES_BASE_URL = "https://wso2-auth.tacc.utexas.edu/store/site/blocks"

# App scpecific configurations:
APP_BASE = 'http://wso2-auth.tacc.utexas.edu'
APP_TENANT_ID = '1'
USE_APP_TENANT_ID = True
NEW_ACCOUNT_EMAIL_SUBJECT='New wso2-agave-staging Account Requested'
NEW_ACCOUNT_FROM = 'do-not-reply@wso2-agave-staging.org'

INACTIVE_STATUS = 'Inactive'
ACTIVE_STATUS = 'Active'

# JWT Header:
# JWT_HEADER = 'HTTP_X_JWT_ASSERTION_WSO2_AGAVE_STAGING'
JWT_HEADER = 'HTTP_X_JWT_ASSERTION_VDJSERVER_ORG'
# CHECK_JWT = False
CHECK_JWT = True
PUB_KEY = '/home/jstubbs/agave-ldap/src/users/usersApp/agave-vdjserver-org_pub.txt'
USER_ADMIN_ROLE = 'Internal/user-account-manager'

# Tenantid key in WSO2 JWT Header:
WSO2_TID_STR = 'enduserTenantId":"'
                                                                          1,9           Top

